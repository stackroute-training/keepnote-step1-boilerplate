﻿using System.Collections.Generic;
using Keepnote_Step1.Models;
using System.Linq;
namespace Keepnote_Step1.Repository
{
      /*
      This class contains the code for data storage interactions and methods 
      of this class will be used by other parts of the applications such
      as Controllers and Test Cases
      */
    public class NoteRepository : INoteRepository
    {
        /* Declare a variable of List type to store all the notes. */
        static List<Note> lstNotes;

         static NoteRepository()
        {
            /* Initialize the variable using proper data type */
            lstNotes = new List<Note>();
        }

        /* This method should return all the notes in the list */
        public List<Note> GetNotes()
        {
            return lstNotes;
        }

        /*
	        This method should accept Note object as argument and add the new note object into  list
	    */
        public void AddNote(Note note)
        {
            lstNotes.Add(note);
        }

        /* This method should deleted a specified note from the list */
        public bool DeletNote(int noteId)
        {
            bool status = Exists(noteId);
            if (status)
            {
                lstNotes.RemoveAll(x => x.NoteId == noteId);
                return true;
            }
            return false;
        }

        /*
	      This method should check if the matching note id present in the list or not.
	      Return true if note id exists in the list or return false if note id does not
	      exists in the list
	  */
        public bool Exists(int noteId)
        {
            List<Note> objList = new List<Note>();
            objList = lstNotes.Where(x => x.NoteId == noteId).ToList();
            if (objList != null && objList.Count > 0)
                return true;
            return false;
        }
    }
}
